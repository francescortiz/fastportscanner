#include "scan.h"

Scan::Scan()
{
}
Scan::Scan(bool stop, QString ip_start, QString ip_end, int port_start, int port_end, int delay, int wait){
    this->stop = stop;
    this->next_address = ip_start;
    this->next_port = port_start;
    this->start_address = ip_start;
    this->start_port = port_start;
    this->end_address = ip_end;
    this->end_port =  port_end;
    this->delay = delay;
    this->wait = wait;
    num_threads = 0;
}
