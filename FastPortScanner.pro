#-------------------------------------------------
#
# Project created by QtCreator 2013-10-31T16:08:14
#
#-------------------------------------------------

QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FastPortScanner
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    executionthread.cpp \
    scan.cpp

HEADERS  += mainwindow.h \
    executionthread.h \
    scan.h

FORMS    += mainwindow.ui


RC_FILE = app.rc
