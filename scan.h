#ifndef SCAN_H
#define SCAN_H
#include <QString>
#include <QMutex>

class Scan
{
public:
    QString next_address;
    int next_port;
    QString start_address;
    int start_port;
    QString end_address;
    int end_port;
    int delay;
    int wait;
    QMutex mutex;
    bool stop;
    int num_threads;

public:
    Scan();
    Scan(bool stop, QString ip_start, QString ip_end, int port_start, int port_end, int delay, int wait);
};

#endif // SCAN_H
