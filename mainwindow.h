#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>

#include "ExecutionThread.h"
#include "scan.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    void load_init_file();
    void save_init_file();
private slots:
    void on_button_start_clicked();

    void on_button_stop_clicked();

public slots:
    void onProgressChagned(QString host, int port, QString info);

private:
    Ui::MainWindow *ui;
    bool stop;
    int port;
    Scan *scan;
};

#endif // MAINWINDOW_H
