#ifndef EXECUTIONTHREAD_H
#define EXECUTIONTHREAD_H

#include <QThread>
#include <QMutex>
#include <QtNetwork>
#include <scan.h>
#ifdef Q_OS_WIN
#include <windows.h> // for Sleep
#endif

void qSleep(int ms);

class ExecutionThread : public QThread
{
    Q_OBJECT
private:

    bool *stop;
    QMutex mutex;
    Scan *scan;


protected:
    void run();
public:
    explicit ExecutionThread(QObject *parent = 0);
    ExecutionThread(Scan *scan, int start_delay);
    QString get_next_address();
    QString suma1ip(QString ip);
    int start_delay;

signals:
   void progressChanged(QString host,int port, QString info);

public slots:

};

#endif // EXECUTIONTHREAD_H
