#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->button_stop->setEnabled(false);
    this->setWindowTitle("Fast Port Scanner");
    load_init_file();
}

MainWindow::~MainWindow()
{
    save_init_file();
    delete ui;
}

void MainWindow::load_init_file(){
    //QDir::setCurrent("y://");qDebug() << QDir::currentPath();// THE EXECUTABLE PATH IS NOT WRITEABLE WHEN IN QTCREATOR


    QFile file("settings.ini");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        return;
    }


    QTextStream in(&file);
    int cnt=0;
    while (!in.atEnd()) {
        QString line = in.readLine();
        if (cnt==0)ui->edit_ip_start->setText(line);
        if (cnt==1)ui->edit_ip_end->setText(line);
        if (cnt==2)ui->edit_port_start->setText(line);
        if (cnt==3)ui->edit_port_end->setText(line);
        if (cnt==4)ui->edit_start_delay->setText(line);
        if (cnt==5)ui->edit_delay_connections->setText(line);
        if (cnt==6)ui->edit_wait->setText(line);
        if (cnt==7)ui->edit_threads->setText(line);
        cnt++;
    }
    file.close();



}

void MainWindow::save_init_file(){
    //QDir::setCurrent("y://");qDebug() << QDir::currentPath();// THE EXECUTABLE PATH IS NOT WRITEABLE WHEN IN QTCREATOR

    QFile file("settings.ini");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        return;
    }

    QTextStream out(&file);
    out << ui->edit_ip_start->text() << "\n";
    out << ui->edit_ip_end->text() << "\n";
    out << ui->edit_port_start->text() << "\n";
    out << ui->edit_port_end->text() << "\n";
    out << ui->edit_start_delay->text() << "\n";
    out << ui->edit_delay_connections->text() << "\n";
    out << ui->edit_wait->text() << "\n";
    out << ui->edit_threads->text() << "\n";
    file.close();

}



void MainWindow::on_button_start_clicked()
{
//    QString address = ui->edit_ip->text();
    int threads = ui->edit_threads->text().toInt(NULL,10);;
//    int delayInt= delay.toInt(NULL,10);
    port = 0;
    scan = new Scan( false,
                     ui->edit_ip_start->text(),
                     ui->edit_ip_end->text(),
                     ui->edit_port_start->text().toInt(NULL,10),
                     ui->edit_port_end->text().toInt(NULL,10),
                     ui->edit_delay_connections->text().toInt(NULL,10),
                     ui->edit_wait->text().toInt(NULL,10)
                   );
    int start_delay = 0;
    for (int i  = 0; i < threads; i++){
          ExecutionThread *thread = new ExecutionThread(scan, start_delay);
          connect(thread, SIGNAL(progressChanged(QString, int, QString)), SLOT(onProgressChagned(QString, int, QString)));
          thread->start();
          start_delay = start_delay + ui->edit_start_delay->text().toInt(NULL,10);
    }

    ui->button_start->setEnabled(false);
    ui->button_stop->setEnabled(true);
}

void MainWindow::on_button_stop_clicked()
{
    scan->stop=true;
    ui->button_start->setEnabled(true);
    ui->button_stop->setEnabled(false);

}

void MainWindow::onProgressChagned(QString host, int port, QString info) {
    // Processing code
    ui->edit_scanning_ip->setText(host);
    ui->edit_scanning_port->setText(QString::number(port));
    if (info == "success"){
        ui->edit_results->append( host + " - " + QString::number(port));
    }
    ui->edit_num_threads->setText(QString::number(scan->num_threads));
    if (scan->num_threads == 0) {
        ui->button_start->setEnabled(true);
        ui->button_stop->setEnabled(false);
    }

//    qDebug() << info;
}
