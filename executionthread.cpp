#include "executionthread.h"
#include <QDebug>

ExecutionThread::ExecutionThread(QObject *parent) :
    QThread(parent)
{

}
ExecutionThread::ExecutionThread(Scan *scan, int start_delay){
    this->scan = scan;
    this->start_delay =  start_delay;
}




void ExecutionThread::run(){

    qSleep(start_delay);

    scan->mutex.lock();
    scan->num_threads++;
    scan->mutex.unlock();

    QTcpSocket *tcpSocket = new QTcpSocket();


    while (!scan->stop){
        QString current_address = get_next_address();
        if (current_address == "stop") break;

        QStringList list = current_address.split(":");
        QString host = list[0];
        int port = list[1].toInt(NULL,10);

        QHostAddress addr(host);

        tcpSocket->connectToHost(addr, port);
        if (tcpSocket->waitForConnected(scan->wait)){
            emit progressChanged(host, port, "success");
            tcpSocket->close();
        }else{
            emit progressChanged(host, port, "failed");
        }
        qSleep(scan->delay);

    }

    scan->mutex.lock();
    scan->num_threads--;
    scan->mutex.unlock();
}

QString ExecutionThread::get_next_address(){

    scan->mutex.lock();

    QString current_address = scan->next_address;
    int current_port = scan->next_port;

    scan->next_address = suma1ip(current_address);

    if (current_port == scan->end_port + 1){
        scan->mutex.unlock();
        return QString("stop");
    }

    if (current_address == scan->end_address) {
        scan->next_address = scan->start_address;
        scan->next_port++;
    }

    scan->mutex.unlock();


    return current_address+":"+QString::number(current_port);
}

QString ExecutionThread::suma1ip(QString ip){

    QStringList list = ip.split(".");
    int a = list[0].toInt(NULL,10);
    int b = list[1].toInt(NULL,10);
    int c = list[2].toInt(NULL,10);
    int d = list[3].toInt(NULL,10);

    int carry = 0;
    d = d + 1;
    if (d>255){
        carry = 1;
        d=0;
    }else{
        return  (QString::number(a) +"."+QString::number(b)+"."+QString::number(c)+"."+QString::number(d));
    }

    c = c + carry;
    if (c>255){
        carry = 1;
        c=0;
    }else{
        return  (QString::number(a) +"."+QString::number(b)+"."+QString::number(c)+"."+QString::number(d));
    }
    b = b + carry;
    if (b>255){
        carry = 1;
        b=0;
    }else{
        return  (QString::number(a) +"."+QString::number(b)+"."+QString::number(c)+"."+QString::number(d));
    }
    a = a + carry;


    if (a>255){
        return(QString("stop"));
    }

    return  (QString::number(a) +"."+QString::number(b)+"."+QString::number(c)+"."+QString::number(d));

}

void qSleep(int ms)
{
//    QTEST_ASSERT(ms > 0);

#ifdef Q_OS_WIN
    Sleep(uint(ms));
#else
    struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
    nanosleep(&ts, NULL);
#endif
}
